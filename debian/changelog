r-cran-rstanarm (2.32.1-2) unstable; urgency=medium

  * Build-Depends: r-cran-v8 (See 
    https://github.com/stan-dev/rstanarm/issues/619#issuecomment-2048300103)
    Closes: #1065339

 -- Andreas Tille <tille@debian.org>  Thu, 11 Apr 2024 07:24:45 +0200

r-cran-rstanarm (2.32.1-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 01 Feb 2024 18:16:58 +0100

r-cran-rstanarm (2.26.1-2) unstable; urgency=medium

  * Follow suggestion of test suite
     parser failed badly; maybe try installing the V8 package
    -> Test-Depends: r-cran-v8
  * Run autopkgtest under xvfb-run

 -- Andreas Tille <tille@debian.org>  Fri, 24 Nov 2023 21:44:24 +0100

r-cran-rstanarm (2.26.1-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * Lintian-overrides for custom-library-search-path
  * Fix clean target
    Closes: #1048873

 -- Andreas Tille <tille@debian.org>  Fri, 27 Oct 2023 18:32:23 +0200

r-cran-rstanarm (2.21.4-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)
  * lintian-overrides (see lintian bug #1017966)

 -- Andreas Tille <tille@debian.org>  Tue, 27 Jun 2023 06:18:43 +0200

r-cran-rstanarm (2.21.3-2) unstable; urgency=medium

  * Team Uploaded.
  * Rebuild with updated rstan (Closes: #1011222)

 -- Nilesh Patra <nilesh@debian.org>  Sun, 04 Sep 2022 13:44:51 +0530

r-cran-rstanarm (2.21.3-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Tue, 24 May 2022 11:24:55 +0200

r-cran-rstanarm (2.21.1-1) unstable; urgency=high

  [ Andreas Tille ]
  * New upstream version
  * debhelper-compat 13 (routine-update)
  * Versioned Build-Depends: r-cran-rcppparallel (>= 5.0.2+dfsg-3)
    Closes: #963392

  [ Shayan Doust ]
  * RcppParallel shared object is under libs/ not lib/
  * Patch src/Makevars to point to correct RcppParallel path
  * Add r-cran-hsaur3 as a test dependency to rectify test fault
  * Reintroduce .Platform$r_arch within test_stan_functions.R
  * Build-Depends: r-cran-rstan (>= 2.21.2-3)

 -- Shayan Doust <hello@shayandoust.me>  Mon, 08 Feb 2021 19:42:38 +0000

r-cran-rstanarm (2.19.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Wed, 15 Apr 2020 14:04:02 +0200

r-cran-rstanarm (2.19.2-2) unstable; urgency=medium

  * Team upload.
  * Add r-base-dev to test-deps.
  * Standards-Version: 4.5.0

 -- Dylan Aïssi <daissi@debian.org>  Tue, 11 Feb 2020 07:01:55 +0100

r-cran-rstanarm (2.19.2-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.4.1
  * Trim trailing whitespace.
  * Set upstream metadata fields: Archive, Bug-Database.
  * Test-Depends: r-cran-codetools

 -- Andreas Tille <tille@debian.org>  Mon, 07 Oct 2019 17:58:15 +0200

r-cran-rstanarm (2.18.2-1) unstable; urgency=medium

  * Initial release (closes: #935493)

 -- Andreas Tille <tille@debian.org>  Fri, 23 Aug 2019 09:37:14 +0200
